object vMain: TvMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'clPick'
  ClientHeight = 242
  ClientWidth = 105
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LabelDesc: TLabel
    AlignWithMargins = True
    Left = 2
    Top = 229
    Width = 103
    Height = 13
    Margins.Left = 2
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alBottom
    Caption = 'F1: Help'
    ExplicitLeft = 0
    ExplicitTop = 253
    ExplicitWidth = 40
  end
  object ListColors: TListBox
    Left = 0
    Top = 105
    Width = 105
    Height = 124
    Align = alClient
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 0
    ExplicitHeight = 148
  end
  object PanelMag: TPanel
    Left = 0
    Top = 0
    Width = 105
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    Caption = 'PanelMag'
    TabOrder = 1
  end
  object ActionManager: TActionManager
    Left = 24
    Top = 120
    StyleName = 'Platform Default'
    object ActionMag1: TAction
      Tag = 2
      Caption = 'x2'
      ShortCut = 16433
      OnExecute = ActionMagX
    end
    object ActionMag2: TAction
      Tag = 4
      Caption = 'x4'
      ShortCut = 16434
      OnExecute = ActionMagX
    end
    object ActionMag3: TAction
      Tag = 8
      Caption = 'x8'
      ShortCut = 16435
      OnExecute = ActionMagX
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 16421
      OnExecute = ActionMouseMove
    end
    object Action2: TAction
      Tag = 1
      Caption = 'Action2'
      ShortCut = 16423
      OnExecute = ActionMouseMove
    end
    object Action3: TAction
      Tag = 2
      Caption = 'Action3'
      ShortCut = 16422
      OnExecute = ActionMouseMove
    end
    object Action4: TAction
      Tag = 3
      Caption = 'Action4'
      ShortCut = 16424
      OnExecute = ActionMouseMove
    end
    object ActionCopy: TAction
      ShortCut = 16451
      OnExecute = ActionCopyExecute
    end
    object ActionSelectAll: TAction
      Caption = 'ActionSelectAll'
      ShortCut = 16449
      OnExecute = ActionSelectAllExecute
    end
    object ActionClear: TAction
      Caption = 'ActionClear'
      ShortCut = 27
      OnExecute = ActionClearExecute
    end
    object ActionDelete: TAction
      Caption = 'ActionDelete'
      ShortCut = 46
      OnExecute = ActionDeleteExecute
    end
    object ActionHelp: TAction
      Caption = 'ActionHelp'
      ShortCut = 112
      OnExecute = ActionHelpExecute
    end
    object ActionCaptureColor: TAction
      Caption = 'ActionCaptureColor'
      OnExecute = ActionCaptureColorExecute
    end
  end
  object Timer: TTimer
    Interval = 50
    OnTimer = TimerTimer
    Left = 72
    Top = 120
  end
end
