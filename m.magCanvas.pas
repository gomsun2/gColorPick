unit m.magCanvas;

interface

uses
  System.Classes, System.SysUtils, System.UITypes, WinApi.Windows, WinApi.Messages,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms
  ;

type
  TMagnifyCanvas = class(TCustomControl)
  private
    FCursor: TCursor;
    FBg: TBitmap;
    FCap: TBitmap;
    FEmpty: Boolean;
    FMag: Integer;
    FCx: Integer;
    FCy: Integer;
    FCaptureCursor: Boolean;
    function GetCenterColor: TColor;
    procedure WmSize(var Msg: TWMSize); message WM_SIZE;
    procedure SetMag(const Value: Integer);
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure CaptureCursor;

    property Mag: Integer read FMag write SetMag;
    property cx: Integer read FCx;
    property cy: Integer read FCy;
    property clCenter: TColor read GetCenterColor;
    property DisplayCursor: Boolean read FCaptureCursor;
    property Empty: Boolean read FEmpty;
  end;

implementation

uses
  System.Math, System.Types
  ;

{ TSimpleCanvas }

procedure TMagnifyCanvas.CaptureCursor;
var
  HDskTop: THandle;
  LCurPos: TPoint;
  HCur: HICON;
  LCanvas: TCanvas;
  LSrc: TRect;
  t: String;
  th,tw, dx, dy: Integer;
  LRect: TRect;
  function xorColor(BackgroundColor: TColor): TColor;
  begin
    BackgroundColor := ColorToRGB(BackgroundColor);
    Result := RGB(
      IfThen(GetRValue(BackgroundColor) > $40, $00, $FF),
      IfThen(GetGValue(BackgroundColor) > $40, $00, $FF),
      IfThen(GetBValue(BackgroundColor) > $40, $00, $FF)
    );
  end;
begin
  HDskTop := GetDesktopWindow;
  GetCursorPos(LCurPos);
  if not Application.MainForm.BoundsRect.Contains(LCurPos) then
  begin
    LCanvas := TCanvas.Create;
    LCanvas.Handle := GetDc(HDskTop);
    try
      dx := Width div 2 div FMag;
      dy := Height div 2 div FMag;
      LSrc := TRect.Create(LCurPos, LCurPos);
      LSrc.Inflate(dx, dy);
      FCap.Canvas.CopyRect(ClientRect, LCanvas, LSrc);
      if FCaptureCursor then
      begin
        HCur := Screen.Cursors[crDefault];
        DrawIconEx(FCap.Canvas.Handle, cx + 1, cy +1, HCur, 32 * FMag, 32 * FMag, 0, 0, DI_NORMAL);
      end;
      FCap.Canvas.Brush.Color := xorColor(clCenter);
      FCap.Canvas.Font.Color := clCenter;
      t := 'x'+FMag.ToString;
      tw := FCap.Canvas.TextWidth(t);
      th := FCap.Canvas.TextHeight(t);
      LRect := TRect.Create(Point(Width-2, 2));
      FCap.Canvas.TextOut(LRect.Left -tw, LRect.Top, t);

      t := '#' + Integer(clCenter).ToHexString(8);
      LRect.SetLocation(2, Height-th-2);
      FCap.Canvas.TextOut(LRect.Left, LRect.Top, t);

      LRect.SetLocation(Width-th-2, Height-th-2);
      LRect.Inflate(0,0, th+2, th+2);
      FCap.Canvas.Rectangle(LRect);

      FCap.Canvas.Brush.Color := clCenter;
      LRect.Inflate(-2, -2);
      FCap.Canvas.Rectangle(LRect);
    finally
      ReleaseDC(HDskTop, LCanvas.Handle);
      FreeAndNil(LCanvas);
    end;
    if FEmpty then
      FEmpty := False;
    Paint;
  end;
end;

constructor TMagnifyCanvas.Create(AOwner: TComponent);
  procedure MakeCanvasBrushBitmap;
  const
    BrushBmpR = 8;
    BrushBmpW = BrushBmpR * 2;
    BrushBmpColorF = clSilver;
    BrushBmpColorB = clGray;
    BrushBmpColor: array[0..1, 0..1] of TColor = (
      (BrushBmpColorF, BrushBmpColorB),
      (BrushBmpColorB, BrushBmpColorF)
    );
  var
    x, y: Integer;
  begin
    FBg := TBitmap.Create;
    FBg.Width := BrushBmpW;
    FBg.Height:= BrushBmpW;
    for x := 0 to BrushBmpW -1 do
      for y:= 0 to BrushBmpW -1 do
        FBg.Canvas.Pixels[x, y] := BrushBmpColor[(x div BrushBmpR) and $1, (y div BrushBmpR) and $1];
  end;
begin
  inherited;

  DoubleBuffered := True;
  FEmpty := True;
  FCaptureCursor := True;
  MakeCanvasBrushBitmap;

  FCap := TBitmap.Create;
  FCap.PixelFormat := pf32bit;
  FCap.Canvas.Brush.Style := bsSolid;
  FCap.Canvas.Pen.Style := psClear;
  FCap.Canvas.Font.Style := [fsBold];

  FCursor := crArrow;

  FMag := 2;
  FCx := Width div 2;
  FCy := Height div 2;
end;

destructor TMagnifyCanvas.Destroy;
begin
  FreeAndNil(FCap);
  FreeAndNil(FBg);

  inherited;
end;

function TMagnifyCanvas.GetCenterColor: TColor;
begin
  if FEmpty then
    Result := $00000000
  else
    Result := FCap.Canvas.Pixels[cx, cy]
end;

procedure TMagnifyCanvas.Paint;
begin
  if not Visible then
    Exit;

  with Canvas do
    if not FEmpty then
      BrushCopy(ClientRect, FCap, ClientRect, $00000001)
    else
    begin
      Pen.Style :=psClear;
      Brush.Bitmap:=FBg;
      Rectangle(Rect(0, 0, Width+1, Height+1)); // 브러쉬만 쓰기 때문에 한칸 늘여서 그려줘야 한다.
    end;
end;

procedure TMagnifyCanvas.SetMag(const Value: Integer);
begin
  if FMag <> Value then
  begin
    FMag := Value;
    Paint;
  end;
end;

procedure TMagnifyCanvas.WmSize(var Msg: TWMSize);
begin
  FCap.SetSize(Msg.Width, Msg.Height);
  FCx := Width div 2;
  FCy := Height div 2;
end;

end.
